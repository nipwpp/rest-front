import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Constants} from '../constants';
import {Post} from '../models/post.model';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient: HttpClient) { }

  getPosts() {
    return this.httpClient.get<Post[]>(Constants.API_URL + '/posts');
  }

  postPosts(post) {
    return this.httpClient.post(Constants.API_URL + '/posts', post);
  }

  putPosts(post: Post) {
    return this.httpClient.put(Constants.API_URL + '/posts/1', post);
  }

  deletePosts() {
    return this.httpClient.delete(Constants.API_URL + '/posts/1');
  }
}
