import {Component, OnInit} from '@angular/core';
import {HttpService} from './services/http.service';
import {Post} from './models/post.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'nipwpp-projekt';

  posts = new Array<Post>();

  private post: Post = {userId: 5, id: 101, title: 'SomeTitle', body: 'somebody'};

  constructor(private httpService: HttpService) {

  }

  loadData() {
    this.httpService.getPosts().subscribe(data => {
      this.posts = data;
    });
  }

  addPost() {
    this.httpService.postPosts(this.post).subscribe(data => {
      console.log(data);
    });
  }

  ngOnInit(): void {
    this.loadData();
  }

  deletePost() {
    this.httpService.deletePosts().subscribe(data => {
      console.log(data);
    });
  }

  updatePost() {
    this.httpService.putPosts(this.post).subscribe(data => {
      console.log(data);
    });
  }
}
